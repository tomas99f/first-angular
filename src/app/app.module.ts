import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { BannerComponent } from './components/banner/banner.component';
import { GaleriaComponent } from './components/galeria/galeria.component';
import { HistoriaComponent } from './components/historia/historia.component';
import { FooterComponent } from './components/footer/footer.component';
import { PlantillaComponent } from './components/plantilla/plantilla.component';
import { GoleadoresComponent } from './components/goleadores/goleadores.component'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BannerComponent,
    GaleriaComponent,
    HistoriaComponent,
    FooterComponent,
    PlantillaComponent,
    GoleadoresComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
