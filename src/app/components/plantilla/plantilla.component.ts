import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plantilla',
  templateUrl: './plantilla.component.html',
  styleUrls: ['./plantilla.component.css']
})
export class PlantillaComponent implements OnInit {
  listaJugadores: any[];
  jugadorSeleccionado: any;

  constructor() { }

  ngOnInit(): void {
    this.llenarJugadores();
    this.jugadorSeleccionado = {
      nro: 0,
      nombre: "",
      posicion: "",
      foto: ""
    }
  }

  llenarJugadores(){
    this.listaJugadores = [
      {"nro":4 , "nombre": "Elías López", "posicion": "Defensor Central", "foto": "Lopez.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":24 , "nombre": "Franco Zabala", "posicion": "Defensor Central", "foto": "Zabala.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":13 , "nombre": "Lucas Romano", "posicion": "Defensor Central", "foto": "Romano.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":3 , "nombre": "Gonzalo Schmidt", "posicion": "Lateral Izquierdo", "foto": "Schmidt.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":26 , "nombre": "Silvio Gonzalez", "posicion": "Lateral Derecho", "foto": "Gonzales.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":5 , "nombre": "Agustín Catalán", "posicion": "Mediocampista Defensivo", "foto": "ACatalan.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":12 , "nombre": "Luciano Ferrera", "posicion": "Mediocampista Creativo", "foto": "Ferrera.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":17 , "nombre": "Cristian Ielpo", "posicion": "Mediocampita Creativo", "foto": "Ielpo.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":10 , "nombre": "Agustín Pueblas", "posicion": "Mediocampista Ofensivo", "foto": "Pueblas.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":7 , "nombre": "Ariel Sack", "posicion": "Mediocampista Ofensivo", "foto": "Sack.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":23 , "nombre": "Tomas Catalán", "posicion": "Delantero Extremo", "foto": "Catalan.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":8 , "nombre": "Lautaro Lamas", "posicion": "Delantero Extremos", "foto": "Lamas.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":27 , "nombre": "Guido Lapilover", "posicion": "Delantero Centro", "foto": "Lapilover.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":14 , "nombre": "Santiago Marfil", "posicion": "Delantero Centro", "foto": "Marfil.jpg", "apodo": "", "pie": "Derecho"},
      {"nro":9 , "nombre": "Sebastian Ludueña", "posicion": "Delantero Centro", "foto": "Ludueña.jpg", "apodo": "", "pie": "Derecho"},
]

  }

  abrirJugador(jugador) {
    this.jugadorSeleccionado = jugador;
    //alert("jugador: "+jugador.nombre+ ", posicion: "+jugador.posicion);
  }

}
