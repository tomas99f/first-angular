import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-goleadores',
  templateUrl: './goleadores.component.html',
  styleUrls: ['./goleadores.component.css']
})
export class GoleadoresComponent implements OnInit {
  listaJugadores: any[];

  constructor() { }

  ngOnInit(): void {
    this.llenarJugadores();
  }

  llenarJugadores(){
    this.listaJugadores = [
	    {"nombre": "Sebastian Ludueña", "goles": "9"},
	    {"nombre": "Lautaro Lamas", "goles": "4"},
	    {"nombre": "Santiago Marfil", "goles": "4"},
	    {"nombre": "Tomas Catalan", "goles": "4"},
    ]
  }

}
